-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 25, 2020 at 06:11 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `finalyearproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `city_place`
--

CREATE TABLE `city_place` (
  `id` int(11) NOT NULL,
  `message` text NOT NULL,
  `imagename` varchar(100) NOT NULL,
  `city_id` int(42) NOT NULL,
  `title` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `discussion`
--

CREATE TABLE `discussion` (
  `id` int(11) NOT NULL,
  `discussion_form_id` int(42) NOT NULL,
  `message_text` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `username` varchar(100) NOT NULL,
  `userimage` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `discussion_forum`
--

CREATE TABLE `discussion_forum` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `family`
--

CREATE TABLE `family` (
  `location` text NOT NULL,
  `instruction` text NOT NULL,
  `phonenumber` varchar(100) NOT NULL,
  `service_type` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `family`
--

INSERT INTO `family` (`location`, `instruction`, `phonenumber`, `service_type`, `username`, `email`, `id`) VALUES
('w', 'w', '03410706570', 'food', 'hassan', 'hassan@gmail.com', 1),
('safsafsa', 'asdfasfasdfsa', '', 'all', 'hassan', 'hassan@gmail.com', 2);

-- --------------------------------------------------------

--
-- Table structure for table `guide_payment`
--

CREATE TABLE `guide_payment` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item_price` float(10,2) NOT NULL,
  `item_price_currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `paid_amount` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `paid_amount_currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `txn_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `guide_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `guide_payment`
--

INSERT INTO `guide_payment` (`id`, `name`, `email`, `item_name`, `item_number`, `item_price`, `item_price_currency`, `paid_amount`, `paid_amount_currency`, `txn_id`, `payment_status`, `created`, `modified`, `guide_name`) VALUES
(11, 'ayesha', 'ayeshaiqbal@gmail.com', 'Guide Fee', 'ayan', 100.00, 'USD', '100', 'usd', 'txn_1I0VD5B9qvzerqLQo45h5p8x', 'succeeded', '2020-12-20 08:56:46', '2020-12-20 08:56:46', ''),
(12, 'ayesha', 'ayeshaiqbal@gmail.com', 'Guide Fee', 'ayan', 100.00, 'USD', '100', 'usd', 'txn_1I2CpcB9qvzerqLQWcaMA7R6', 'succeeded', '2020-12-25 01:43:40', '2020-12-25 01:43:40', ''),
(13, 'ayesha', 'ayeshaiqbal@gmail.com', 'Guide Fee', 'ayan', 100.00, 'USD', '100', 'usd', 'txn_1I2Cq4B9qvzerqLQCiTcwxab', 'succeeded', '2020-12-25 01:44:08', '2020-12-25 01:44:08', ''),
(14, 'ayesha', 'ayeshaiqbal@gmail.com', 'Guide Fee', 'ayan', 100.00, 'USD', '100', 'usd', 'txn_1I2D2VB9qvzerqLQCRggiLuE', 'succeeded', '2020-12-25 01:56:59', '2020-12-25 01:56:59', ''),
(15, 'ayesha', 'ayeshaiqbal@gmail.com', 'Guide Fee', 'ayan', 100.00, 'USD', '100', 'usd', 'txn_1I2Dh9B9qvzerqLQyOT4hGCg', 'succeeded', '2020-12-25 02:38:59', '2020-12-25 02:38:59', '');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(60) NOT NULL,
  `user_id` int(60) NOT NULL,
  `image` binary(120) NOT NULL,
  `image_text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item_price` float(10,2) NOT NULL,
  `item_price_currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `paid_amount` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `paid_amount_currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `txn_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `name`, `email`, `item_name`, `item_number`, `item_price`, `item_price_currency`, `paid_amount`, `paid_amount_currency`, `txn_id`, `payment_status`, `created`, `modified`) VALUES
(21, 'ayesha', 'ayeshaiqbal@gmail.com', 'Basic Plan', 'PN12345', 20.00, 'USD', '20', 'usd', 'txn_1I0V3uB9qvzerqLQFh2c3076', 'succeeded', '2020-12-20 08:47:17', '2020-12-20 08:47:17'),
(22, 'ayan', 'ayan@gmail.com', 'Gold Plan', 'PN12345', 60.00, 'USD', '60', 'usd', 'txn_1I0V9oB9qvzerqLQDP2Gx0t3', 'succeeded', '2020-12-20 08:53:23', '2020-12-20 08:53:23'),
(23, 'hassan', 'hassan@gmail.com', 'Basic Plan', 'PN12345', 20.00, 'USD', '20', 'usd', 'txn_1I1FXZB9qvzerqLQOvmWiMyH', 'succeeded', '2020-12-22 10:25:02', '2020-12-22 10:25:02'),
(24, 'hassan', 'hassan@gmail.com', 'Basic Plan', 'PN12345', 20.00, 'USD', '20', 'usd', 'txn_1I1FZIB9qvzerqLQuFaZSh7a', 'succeeded', '2020-12-22 10:26:49', '2020-12-22 10:26:49'),
(25, 'hassan', 'hassan@gmail.com', 'Basic Plan', 'PN12345', 20.00, 'USD', '20', 'usd', 'txn_1I1FnyB9qvzerqLQRFnK2wGL', 'succeeded', '2020-12-22 10:42:00', '2020-12-22 10:42:00');

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `id` int(11) NOT NULL,
  `knowledge` int(11) NOT NULL,
  `behavour` int(11) NOT NULL,
  `clearness` int(11) NOT NULL,
  `follow_time` int(11) NOT NULL,
  `Cheerful` int(11) NOT NULL,
  `name` varchar(400) NOT NULL,
  `given_by` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`id`, `knowledge`, `behavour`, `clearness`, `follow_time`, `Cheerful`, `name`, `given_by`) VALUES
(5, 5, 3, 5, 5, 5, 'ayan', ''),
(6, 5, 3, 5, 5, 5, 'ayan', ''),
(7, 5, 3, 5, 5, 5, 'ayan', 'ayesha'),
(8, 5, 3, 5, 5, 5, 'ayan', 'ayesha'),
(9, 5, 3, 5, 5, 5, 'ayan', 'ayesha'),
(10, 5, 3, 5, 5, 5, 'ayan', 'ayesha'),
(11, 5, 3, 5, 5, 5, 'ayan', 'ayesha'),
(12, 5, 3, 5, 5, 5, 'ayan', 'ayesha'),
(13, 5, 3, 5, 5, 5, 'ayan', 'ayesha'),
(14, 5, 3, 5, 5, 5, 'ayan', 'ayesha'),
(15, 5, 3, 5, 5, 5, 'ayan', 'ayesha'),
(16, 5, 3, 5, 5, 5, 'ayan', 'ayesha'),
(17, 5, 3, 5, 5, 5, 'ayan', 'ayesha'),
(18, 5, 3, 5, 5, 5, 'ayan', 'ayesha'),
(19, 5, 3, 5, 5, 5, 'ayan', 'ayesha'),
(20, 5, 3, 5, 5, 5, 'ayan', 'ayesha');

-- --------------------------------------------------------

--
-- Table structure for table `request_family`
--

CREATE TABLE `request_family` (
  `id` int(11) NOT NULL,
  `requester_name` varchar(100) NOT NULL,
  `family_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `address` varchar(60) NOT NULL,
  `city` varchar(60) NOT NULL,
  `country` varchar(60) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `subscribe` tinyint(1) NOT NULL DEFAULT 0,
  `user_type` varchar(60) NOT NULL,
  `image` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `role` varchar(100) NOT NULL,
  `phonenumber` varchar(60) NOT NULL,
  `facebook` text NOT NULL,
  `gmail` text NOT NULL,
  `website` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `guide_book` tinyint(1) NOT NULL,
  `point` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `address`, `city`, `country`, `username`, `password`, `subscribe`, `user_type`, `image`, `description`, `role`, `phonenumber`, `facebook`, `gmail`, `website`, `created_at`, `guide_book`, `point`) VALUES
(10, 'Ayesha Iqbal', 'ayeshaiqbal@gmail.com', 'Johar town near bahria university Lahore Campus', 'Lahore', 'BB', 'ayesha', 'password', 1, '', 'develop-web-applications-with-react-js-and-nodejs.jpg', '<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus. P', 'user', '', 'https://bitnami.com/stack/xampp?utm_source=bitnami&utm_medium=installer&utm_campaign=XAMPP%2BInstaller', 'sfdhttps://bitnami.com/stack/xampp?utm_source=bitnami&utm_medium=installer&utm_campaign=XAMPP%2BInstaller', 'sdfsfhttps://bitnami.com/stack/xampp?utm_source=bitnami&utm_medium=installer&utm_campaign=XAMPP%2BInstaller', '2020-12-20 16:45:18', 1, 2023),
(11, 'ayan', 'ayan@gmail.com', 'ghazi road lahore cantt', 'Lahore', 'BY', 'ayan', 'password', 1, '', '8.jfif', '<p>Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Sed porttitor lectus nibh. Pro', 'guide', '', 'https://bitnami.com/stack/xampp?utm_source=bitnami&utm_medium=installer&utm_campaign=XAMPP%2BInstaller', 'https://www.google.com/search?sxsrf=ALeKk01OEVxUAyMptKctBN7S0YxIsTupqw%3A1608222884531&ei=pIjbX42GIKPB8gLt87TYAg&q=how+to+check+if+the+record+exists+in+php&oq=how+to+check+if+the+record+exists+in+php&gs_lcp=CgZwc3ktYWIQAzIJCAAQyQMQFhAeMgYIABAWEB4yBggAEBYQHlAAWABgywFoAHAAeACAAZgCiAGYApIBAzItMZgBAKoBB2d3cy13aXo&sclient=psy-ab&ved=0ahUKEwiN6IGOudXtAhWjoFwKHe05DSsQ4dUDCA0&uact=5', 'https://www.google.com/search?sxsrf=ALeKk01OEVxUAyMptKctBN7S0YxIsTupqw%3A1608222884531&ei=pIjbX42GIKPB8gLt87TYAg&q=how+to+check+if+the+record+exists+in+php&oq=how+to+check+if+the+record+exists+in+php&gs_lcp=CgZwc3ktYWIQAzIJCAAQyQMQFhAeMgYIABAWEB4yBggAEBYQHlAAWABgywFoAHAAeACAAZgCiAGYApIBAzItMZgBAKoBB2d3cy13aXo&sclient=psy-ab&ved=0ahUKEwiN6IGOudXtAhWjoFwKHe05DSsQ4dUDCA0&uact=5', '2020-12-20 16:52:44', 0, 50046),
(12, 'hassan', 'hassan@gmail.com', 'ghazi road lahore cantt', 'Lahore', 'AT', 'hassan', 'password', 1, '', '8.jfif', '<div style=\"color: rgb(212, 212, 212); background-color: rgb(30, 30, 30); font-family: Consolas, &qu', 'family', '', 'https://bitnami.com/stack/xampp?utm_source=bitnami&utm_medium=installer&utm_campaign=XAMPP%2BInstaller', 'sfdhttps://bitnami.com/stack/xampp?utm_source=bitnami&utm_medium=installer&utm_campaign=XAMPP%2BInstaller', 'sdfsfhttps://bitnami.com/stack/xampp?utm_source=bitnami&utm_medium=installer&utm_campaign=XAMPP%2BInstaller', '2020-12-22 18:24:22', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city_place`
--
ALTER TABLE `city_place`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discussion`
--
ALTER TABLE `discussion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discussion_forum`
--
ALTER TABLE `discussion_forum`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `family`
--
ALTER TABLE `family`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guide_payment`
--
ALTER TABLE `guide_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_family`
--
ALTER TABLE `request_family`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `city_place`
--
ALTER TABLE `city_place`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `discussion`
--
ALTER TABLE `discussion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `discussion_forum`
--
ALTER TABLE `discussion_forum`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `family`
--
ALTER TABLE `family`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `guide_payment`
--
ALTER TABLE `guide_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(60) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `request_family`
--
ALTER TABLE `request_family`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
