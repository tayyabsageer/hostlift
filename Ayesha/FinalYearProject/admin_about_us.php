<?php
    session_start();
  
?>

<?php include('section/header.php'); ?>
<div class="wrapper">
<?php include('section/nav_top_header_section.php'); ?>
<div class="row" style="margin-top:100px;">
    <div class="col-md-6 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Send Message To Discussion Forum</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="create_city_place.php" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                            <div class="form-group">
                            <label>Select City Name</label>
                            <select class="form-control" name="city_id">
                            <option selected disabled>Select the City Name</option>
                            <?php 
                                $sql = "SELECT * FROM city";
                                if($result = mysqli_query($con, $sql)){
                                if(mysqli_num_rows($result) > 0){
                                    while($row = mysqli_fetch_array($result)){
                                ?>
                                    <option value='<?php echo $row['id']; ?>'><?php echo $row['title']; ?></option>
                                <?php
                                    }
                                }
                            }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                        <label>Place Title</label>
                        <input type="text" name="title", class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="3" name="citytext"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Image</label>
                            <input type="file" name="cityimage">
                        </div>
                        <div class="form-group">
                        <label>Place Location</label>
                        <textarea  name="location" cols="10" rows="4" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">Submit</button>
                    </div>
                </form>
            </div>
        </div>       
    </div>
    <div class="col-md-6 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Create New City</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="create_city.php" method="post">
                <div class="form-group">
                            <label>City Name</label>
                            <input type="text" class="form-control" placeholder="Title" name="title"> 
                            </div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">Submit</button>
                    </div>
                </form>
            </div>
        </div>       
    </div>
</div>
<?php include('section/footer.php'); ?>
