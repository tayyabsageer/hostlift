<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit359f4c8707188b6dfe04bc31a60e22c3
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Stripe\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Stripe\\' => 
        array (
            0 => __DIR__ . '/..' . '/stripe/stripe-php/lib',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit359f4c8707188b6dfe04bc31a60e22c3::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit359f4c8707188b6dfe04bc31a60e22c3::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
