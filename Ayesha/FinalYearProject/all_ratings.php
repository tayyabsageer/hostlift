<?php
    session_start();
    $username = $_SESSION['login'];
    if($username == null)
    {
        header("location:login.php");
    }
  
?>

<?php include('section/header.php'); ?>
<div class="wrapper">
<?php include('section/nav_top_header_section.php'); ?>
<div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption font-dark">
                                                    <i class="icon-settings font-dark"></i>
                                                    <span class="caption-subject bold uppercase">Payment Table</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                                                    <thead>
                                                        <tr class="">
                                                            <th>ID</th>
                                                            <th> Rate BY</th>
                                                            <th> Timeline </th>
                                                            <th> Behaviour</th>
                                                            <th> Cheefulness</th>
                                                            <th> Knowledge</th>
                                                            <th>Clearness</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                       <?php 
                                                            $sql = "SELECT * FROM rating";
                                                          if($result = mysqli_query($con, $sql)){
                                                            if(mysqli_num_rows($result) > 0){
                                                            ?>
                                                        <tr>
                                                           <?php  while($row = mysqli_fetch_array($result)){
                                                            ?>
                                                            <td><?php echo  $row['id']; ?></td>
                                                            <td><?php echo  $row['given_by']; ?> </td>
                                                            <td><?php echo  $row['follow_time']; ?> </td>
                                                            <td><?php echo  $row['behavour']; ?></td>
                                                            <td><?php echo  $row['Cheerful']; ?></td>
                                                            <td><?php echo  $row['knowledge']; ?></td>
                                                            <td><?php echo  $row['clearness']; ?></td>
                                                        
                                                        </tr>
                                                        <?php
                                                           }
                                                            } 
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- END EXAMPLE TABLE PORTLET-->
</div>
<?php include('section/footer.php'); ?>
