<?php include('section/header.php'); ?>
<?php
 session_start();
  if($_SESSION == null)
  {
    header("location:login.php");
  }
?>
<div class="wrapper">
    <?php include('section/nav_top_header_section.php'); ?>
    <?php
   $name = '';
   $username = $_SESSION['login'];
   $email = '';
   $city = '';
   $country = '';
   $address = '';
   $description ='';
   $role = '';
   $image = '';
   $gmail_link ='';   
   $facebook_link ='';
   $website_link ='';
   $phonenumber ='';
   if($_SESSION['id'] > 0)
   {
         $sql = "select * from user WHERE username = '$username' ";
       if($result = mysqli_query($con, $sql))
       {
        if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_assoc($result)){
                $name =$row['name'];
                $email = $row['email'];
                $country = $row['country'];
                $city =$row['city'];
                $address =$row['address'];
                $description = $row['description'];
                $role = $row['role'];
                $image = $row['image'];
                $facebook_link = $row['facebook'];
                $gmail_link = $row['gmail'];
                $website_link = $row['website'];
                $phonenumber = $row['phonenumber'];
            }
        }
        }
   }
   else
   {
      header('location:index.php');
   }
?>
    <div class="page-content-container">
        <div class="page-content-row">
            <div class="page-content-col">
               <?php
               if($_SESSION)
               {
                   
                   if($_SESSION['message']!= null)
                   {
                      ?>
                      <div class="login" id="message">
                                <div class="logo" style= "color:black;backgroud-color:red;">
                                        <div class="desc"><?php echo $_SESSION['message']; ?></div>
                                </div>
                        </div>
            
                    <?php
                   }
               }
               ?>
                <!-- BEGIN PAGE BASE CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PROFILE SIDEBAR -->
                        <div class="profile-sidebar">
                            <!-- PORTLET MAIN -->
                            <div class="portlet light profile-sidebar-portlet bordered">
                                <!-- SIDEBAR USERPIC -->
                                <div class="profile-userpic">
                                    <img src="user/images/<?php echo $_SESSION['image']; ?>" class="img-responsive" alt="" style="    height: 300px;"> </div>
                                <div class="profile-usertitle">
                                    <div class="profile-usertitle-name"><?php echo $name ?></div>
                                    <div class="profile-usertitle-job"><?php echo $role ?> </div>
                                </div>
                                <div class="profile-usermenu">
                                    <ul class="nav">
                                        <li>
                                            <a href="setting.php">
                                                <i class="icon-settings"></i> Account Settings( Change Password, Edit Profile) </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>


                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                            <div>
                                <h4 class="profile-desc-title">About <?php echo $name ?></h4>
                                <span class="profile-desc-text">  <?php echo $description ?> </span>
                                <div class="margin-top-20 profile-desc-link">
                                <b> Phone Number:</b> <i class="fa fa-phone"></i>
                                   <?php echo $phonenumber ?>
                                </div>
                                <div class="margin-top-20 profile-desc-link">
                                <b>City Name :</b><i class="icon-map"></i>
                                   <?php echo $city ?>
                                </div>
                                <div class="margin-top-20 profile-desc-link">
                                <b> Address:</b> <i class="icon-map"></i>
                                   <?php echo $address ?>
                                </div>
                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-globe"></i>
                                    <a href="http://www.keenthemes.com"><?php echo $facebook_link ?></a>
                                </div>
                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-twitter"></i>
                                    <a href="http://www.twitter.com/keenthemes/"><?php echo $gmail_link ?></a>
                                </div>
                                <div class="margin-top-20 profile-desc-link">
                                    <i class="fa fa-facebook"></i>
                                    <a href="http://www.facebook.com/keenthemes/"><?php echo $website_link ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
                    <script>
                        $(document).ready(function() {
                            function readURL(input) {
                                if (input.files && input.files[0]) {
                                    var reader = new FileReader();

                                    reader.onload = function(e) {
                                        $('#imagedisplay').attr('src', e.target.result);
                                    }

                                    reader.readAsDataURL(input.files[0]); // convert to base64 string
                                }
                            }

                            $("#imgInp").change(function() {
                                readURL(this);
                            });
                        });
                    </script>
                    <script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
                    <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                    <script src="../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
                    <script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
                    <script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
                    <script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
                    <!-- END CORE PLUGINS -->
                    <!-- BEGIN PAGE LEVEL PLUGINS -->
                    <script src="../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
                    <script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
                    <script src="../assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
                    <script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
                    <script src="../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
                    <script src="../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
                    <script src="../assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
                    <script src="../assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
                    <script src="./../assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
                    <!-- END PAGE LEVEL PLUGINS -->
                    <!-- BEGIN THEME GLOBAL SCRIPTS -->
                    <script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
                    <!-- END THEME GLOBAL SCRIPTS -->
                    <!-- BEGIN PAGE LEVEL SCRIPTS -->
                    <script src="../assets/pages/scripts/form-validation.min.js" type="text/javascript"></script>
                    <!-- END PAGE LEVEL SCRIPTS -->
                    <!-- BEGIN THEME LAYOUT SCRIPTS -->
                    <script src="../assets/layouts/layout5/scripts/layout.min.js" type="text/javascript"></script>
                    <script src="../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>

                    <!-- END THEME LAYOUT SCRIPTS -->
                    </body>

                    </html>