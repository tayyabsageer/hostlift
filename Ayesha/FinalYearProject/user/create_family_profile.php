<?php 
  session_start();
  ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Family Details</title>
  </head>
  <body class="container-fluid">
  <div class="jumbotron">
  <h3>Enter the Family Details</h3>
  </div>
<div class="col-sm-12 col-md-12">
   <form action="builtfamilyprofile.php" method="post"> 
     <div class="form-group">
        <label><b>Home Location</b></label>
        <textarea name="homelocation" cols="10" rows="3" class="form-control"></textarea>
     </div>
     <div class="form-group">
        <label><b>Instructions</b></label>
        <textarea name="instructions" cols="10" rows="3" class="form-control"></textarea>
     </div>
     <div class="form-group">
        <label><b>Phonenumber</b></label>
        <input type="number" name="phonenumber" class="form-control">
     </div>
     <div class="form-group">
        <label><b>Family Offers</b></label>
        <div class="mt-radio-inline">
        <label class="mt-radio">
        <input type="radio" name="role" id="optionsRadios4" value="food" checked> 
         Food Only
        <span></span>
        </label>
        <label class="mt-radio">
        <input type="radio" name="role" id="optionsRadios5" value="Rentalhome&food"> 
         Rental Home + Food
        <span></span>
        </label>
        <label class="mt-radio">
        <input type="radio" name="role" id="optionsRadios6" value="renthome"> 
          Rental Home
        <span></span>
        </label>
        <label class="mt-radio">
        <input type="radio" name="role" id="optionsRadios6" value="all"> 
          Rental Home + Food + Services(Pick and Drop, Laundary Services )
        <span></span>
        </label>
        </div>
     </div>
     <div class="form-group">
       <button type="submit" class="btn btn-circle btn-primary">Save Family Details</button>
     </div>
   </form>
</div>
  <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>