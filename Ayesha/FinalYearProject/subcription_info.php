<?php
   session_start();
   if($_SESSION['id'] > 0)
   {
      echo "Asd";
   }
   else
   {
      header('location:index.php');
   }
?>
<?php include('section/header.php'); ?>
<div class="wrapper">
<?php include('section/nav_top_header_section.php'); ?>
     <div class="page-content-col">
         <div class="row">
            <div class="col-md-12">
                  <div class="portlet light portlet-fit bordered">
                     <div class="portlet-title">
                        <div class="caption">
                              <i class=" icon-layers font-green"></i>
                              <span class="caption-subject font-green bold uppercase">Subscription Plans</span>
                        </div>
                     </div>
                     <div class="portlet-body">
                        <div class="mt-element-card mt-element-overlay">
                              <div class="row">
                                 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="height: 265px !important;">
                                    <div class="mt-card-item">
                                          <div class="mt-card-avatar mt-overlay-4">
                                             <img src="../assets/pages/img/avatars/team1.jpg"  />
                                             <div class="mt-overlay">
                                                <h2>Basic Plan</h2>
                                                <div class="mt-info font-white">
                                                      <div class="mt-card-content">
                                                         <p class="mt-card-desc font-white"><i class="fa fa-money fa-lg"></i>&nbsp;Price: $20</p>
                                                         <div class="mt-card-social">
                                                            <ul>
                                                                  <li style="margin-top:20px;">
                                                                     <a class="mt-card-btn" href="stripe/index.php?rate=20&plan=Basic Plan">
                                                                     <i class="fa fa-bars fa-lg"></i>&nbsp;Subscription To PLan
                                                                     </a>
                                                                  </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                </div>
                                             </div>
                                          </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="height: 265px !important;">
                                    <div class="mt-card-item">
                                          <div class="mt-card-avatar mt-overlay-4">
                                             <img src="../assets/pages/img/avatars/team2.jpg" />
                                             <div class="mt-overlay">
                                                <h2>Premium Plan</h2>
                                                <div class="mt-info font-white">
                                                      <div class="mt-card-content">
                                                         <p class="mt-card-desc font-white"><i class="fa fa-money fa-lg"></i>&nbsp;$40</p>
                                                         <div class="mt-card-social">
                                                            <ul>
                                                                  <li style="margin-top:20px;">
                                                                     <a class="mt-card-btn" href="stripe/index.php?rate=40&plan=Premium Plan">
                                                                     <i class="fa fa-bars fa-lg"></i>&nbsp;Subscription To PLan
                                                                     </a>
                                                                  </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                </div>
                                             </div>
                                          </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="height: 265px !important;">
                                    <div class="mt-card-item">
                                          <div class="mt-card-avatar mt-overlay-4">
                                             <img src="../assets/pages/img/avatars/team3.jpg" />
                                             <div class="mt-overlay">
                                                <h2>Gold Plan</h2>
                                                <div class="mt-info font-white">
                                                      <div class="mt-card-content">
                                                         <p class="mt-card-desc font-white"><i class="fa fa-money fa-lg"></i>&nbsp;$60</p>
                                                         <div class="mt-card-social">
                                                            <ul>
                                                                  <li style="margin-top:20px;">
                                                                     <a class="mt-card-btn" href="stripe/index.php?rate=60&plan=Gold Plan">
                                                                        <i class="fa fa-bars fa-lg"></i>&nbsp;Subscription To PLan
                                                                     </a>
                                                                  </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                </div>
                                             </div>
                                          </div>
                                    </div>
                                 </div>
                              </div>
                        </div>
                     </div>
                  </div>
            </div>
         </div>
         
      </div>

</div>
<?php include('section/footer.php'); ?>