<?php
    session_start();
    $username = $_SESSION['login'];
    if($username == null)
    {
        header("location:login.php");
    }
  
?>

<?php include('section/header.php'); ?>
<div class="wrapper">
<?php include('section/nav_top_header_section.php'); ?>
<div class="row" style="margin-top:100px;">
    <div class="col-md-6 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Send Message To Discussion Forum</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="discussion/send_message.php" method="post">
                    <div class="form-body">
                            <div class="form-group">
                            <label>Topic of Discussion</label>
                            <select class="form-control" name="discussionid">
                            <option selected disabled>Select the Topic of Discussion</option>
                            <?php 
                                $sql = "SELECT * FROM discussion_forum";
                                if($result = mysqli_query($con, $sql)){
                                if(mysqli_num_rows($result) > 0){
                                    while($row = mysqli_fetch_array($result)){
                                ?>
                                    <option value='<?php echo $row['id']; ?>'><?php echo $row['name']; ?></option>
                                <?php
                                    }
                                }
                            }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Write the Your Comments</label>
                            <textarea class="form-control" rows="3" name="discussiontext"></textarea>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">Submit</button>
                    </div>
                </form>
            </div>
        </div>       
    </div>
    <div class="col-md-6 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Create New Discussion Forum</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="discussion/create_forum.php" method="post">
                <div class="form-group">
                            <label>Title of Discussion Forum</label>
                            <input type="text" class="form-control" placeholder="Title" name="title"> 
                            </div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">Submit</button>
                    </div>
                </form>
            </div>
        </div>       
    </div>
</div>
<?php include('section/footer.php'); ?>
