<?php
    session_start();
    $username = $_SESSION['login'];
    if($username == null)
    {
        header("location:login.php");
    }

  
?>

<?php include('section/header.php'); ?>
<div class="wrapper">
<div class="row">
 <div class="container-fluid">
                <div class="page-content">
                
                    <!-- BEGIN SIDEBAR CONTENT LAYOUT -->
                    <div class="page-content-container">
                        <div class="page-content-row">
            
                            <div class="page-content-col">
                                <!-- BEGIN PAGE BASE CONTENT -->

                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- BEGIN VALIDATION STATES-->
                                        <div class="portlet light portlet-fit portlet-form bordered">
                                            <div class="portlet-body">
                                                <!-- BEGIN FORM-->
                                                <form action="./user/create_profile.php" id="form_sample_3" class="form-horizontal" method="post" enctype="multipart/form-data">
                                                        <div class="form-group last">
                                                            <div class="col-md-12">
                                                            <label>Image</label>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <img src='../../Ayesha/images/admin.jpg' id="imagedisplay" style="height:250px; width:50%;">
                                                                <input type="file" class="form-control" id="imgInp" name="image">
                                                            </div>
                                                            <div class="col-md-12" style="margin-top:20px;">
                                                                <label>User Description</label>
                                                                <textarea class="ckeditor form-control" name="description" rows="6" data-error-container="#editor2_error"></textarea>
                                                                <div id="editor2_error"> </div>
                                                            </div>
                                                        </div
                                                                    <div class="form-group">
                                                            <label>Login As:</label>
                                                            <div class="mt-radio-inline">
                                                                <label class="mt-radio">
                                                                    <input type="radio" name="role" id="optionsRadios4" value="user" checked> 
                                                                      User
                                                                    <span></span>
                                                                </label>
                                                                <label class="mt-radio">
                                                                    <input type="radio" name="role" id="optionsRadios5" value="guide"> 
                                                                      Guide 
                                                                    <span></span>
                                                                </label>
                                                                <label class="mt-radio">
                                                                    <input type="radio" name="role" id="optionsRadios6" value="family"> 
                                                                      Family
                                                                    <span></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12">
                                                        <div class="form-group col-sm-12">
                                                         <label>Facebook Link</label>
                                                         <input type="text" name="facebook",class="form-control" style="width:100%;padding:10px;border-radius:10px;">
                                                        </div>
                                                        <div class="form-group">
                                                         <label>Gmail Link</label>
                                                         <input type="text" name="gmail",class="form-control" style="width:100%;padding:10px;border-radius:10px;">
                                                        </div>
                                                        <div class="form-group">
                                                         <label>Website Link</label>
                                                         <input type="text" name="website",class="form-control" style="width:100%;padding:10px;border-radius:10px;">
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" class="btn green">Submit</button>
                                                                <button type="button" class="btn default">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                            <!-- END VALIDATION STATES-->
                                        </div>
                                    </div>
                                </div>
                                <!-- END PAGE BASE CONTENT -->
                            </div>
                        </div>
                    </div>
                    <!-- END SIDEBAR CONTENT LAYOUT -->
                </div>
                <!-- BEGIN FOOTER -->
    
    
                <!-- END FOOTER -->
            </div>
                                        <!-- END EXAMPLE TABLE PORTLET-->
</div>
        <!-- BEGIN CORE PLUGINS -->
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>        
<script>
            
             $(document).ready(function(){
                function readURL(input) {
                  if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                      $('#imagedisplay').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]); // convert to base64 string
                  }
                }

                $("#imgInp").change(function() {
                  readURL(this);
                }); 
             });
        </script>
        <script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
        <script src="./../assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="../assets/pages/scripts/form-validation.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="../assets/layouts/layout5/scripts/layout.min.js" type="text/javascript"></script>
        <script src="../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>

        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>
