<?php include('section/header.php'); ?>
<div class="wrapper">
    <?php include('section/nav_top_header_section.php'); ?>
    <?php
   $name = '';
   $username = $_SESSION['login'];
   $email = '';
   $city = '';
   $country = '';
   $address = '';
   $description ='';
   $role = '';
   $image = '';
   $gmail_link ='';   
   $facebook_link ='';
   $website_link ='';
   $phonenumber ='';
   if($_SESSION['id'] > 0)
   {
         $sql = "select * from user WHERE username = '$username' ";
       if($result = mysqli_query($con, $sql))
       {
        var_dump($result);
        if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_assoc($result)){
                $name =$row['name'];
                $email = $row['email'];
                $country = $row['country'];
                $city =$row['city'];
                $address =$row['address'];
                $description = $row['description'];
                $role = $row['role'];
                $image = $row['image'];
                $facebook_link = $row['facebook'];
                $gmail_link = $row['gmail'];
                $website_link = $row['website'];
                $phonenumber = $row['phonenumber'];
            }
        }
        }
   }
   else
   {
      header('location:index.php');
   }
?>
    <div class="container-fluid">
        <div class="page-content">
            <!-- BEGIN BREADCRUMBS -->
            <div class="breadcrumbs">
                <h1>Setting</h1>
                <ol class="breadcrumb">
                    <li class="active">User</li>
                </ol>
                <!-- Sidebar Toggle Button -->
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-sidebar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="toggle-icon">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </span>
                </button>
                <!-- Sidebar Toggle Button -->
            </div>
            <!-- END BREADCRUMBS -->
            <!-- BEGIN SIDEBAR CONTENT LAYOUT -->
            <div class="page-content-container">
                <div class="page-content-row">
                    <!-- END PAGE SIDEBAR -->
                    <div class="page-content-col">
                        <!-- BEGIN PAGE BASE CONTENT -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet light bordered">
                                                <div class="portlet-title tabbable-line">
                                                    <div class="caption caption-md">
                                                        <i class="icon-globe theme-font hide"></i>
                                                        <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                                    </div>
                                                    <ul class="nav nav-tabs">
                                                        <li class="active">
                                                            <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                                                        </li>

                                                    </ul>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="tab-content">
                                                        <!-- PERSONAL INFO TAB -->
                                                        <div class="tab-pane active" id="tab_1_1">
                                                            <form role="form" action="user/update_user_info.php" method="post">
                                                                <div class="form-group">
                                                                    <label class="control-label">Username</label>
                                                                    <input type="text" placeholder="Username" class="form-control" value="<?php echo $username; ?>" name="username"/> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">City</label>
                                                                    <input type="text" placeholder="City" class="form-control" name="city"/ value="<?php echo $city; ?>"> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Country</label>
                                                                    <input type="text" placeholder="Country" class="form-control" name="country" value="<?php echo $country; ?>"/> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Name</label>
                                                                    <input type="text" placeholder="Full Name" class="form-control" name="fullname" value="<?php echo $name; ?>"/> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Mobile Number</label>
                                                                    <input type="text" placeholder="Phonenumber" class="form-control"
                                                                           value="<?php echo $phonenumber; ?>" name="phonenumber"/> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">email</label>
                                                                    <input type="text" placeholder="email" class="form-control" value="<?php echo $email; ?>" name="email"/> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Description</label>
                                                                    <textarea class="form-control" rows="3" placeholder="Description"
                                                                    value="<?php echo $description; ?>" name="description"></textarea>
                                                                </div>
                                                                  <div class="form-group">
                                                                    <label class="control-label">Address</label>
                                                                    <textarea class="form-control" rows="3" placeholder="Address"
                                                                    value="<?php echo $address; ?>" name="address"></textarea>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Facebook</label>
                                                                    <input type="text" placeholder="http://www.mywebsite.com" class="form-control" value="<?php echo $facebook_link; ?>" name="facebook" /> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Gmail</label>
                                                                    <input type="text" placeholder="http://www.mywebsite.com" class="form-control" value="<?php echo $gmail_link; ?>" name="gmail" /> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Website</label>
                                                                    <input type="text" placeholder="http://www.mywebsite.com" class="form-control" value="<?php echo $website_link; ?>" name="website"/> </div>
                                                                <div class="margiv-top-10">
                                                                    <button type="submit" class="btn green" name="btnsubmit" value="1"> Save Changes </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!-- END PERSONAL INFO TAB -->
                                                        <!-- CHANGE AVATAR TAB -->
                                                        <div class="tab-pane" id="tab_1_2">
                                                            <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                                                                laborum eiusmod. </p>
                                                            <form action="user/update_user_profile_image.php" role="form" method="post"  enctype="multipart/form-data">
                                                                <div class="form-group">
                                                                    <h1>Previous Image</h1>
                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                            
                                                                            <img src="./user/images/<?php echo $_SESSION['image']; ?>" alt="" /> </div>
                                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                        <div>
                                                                            <span class="btn default btn-file">
                                                                                <span class="fileinput-new"> Select image </span>
                                                                                <span class="fileinput-exists"> Change </span>
                                                                                <input type="file" name="profile_image"> </span>
                                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix margin-top-10">
                                                                        <span class="label label-danger">NOTE! </span>
                                                                        <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                                                    </div>
                                                                </div>
                                                                <div class="margin-top-10">
                                                                    <button type="submit" name="btnsubmit" value="1" class="btn green"> Submit </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!-- END CHANGE AVATAR TAB -->
                                                        <!-- CHANGE PASSWORD TAB -->
                                                        <div class="tab-pane" id="tab_1_3">
                                                            <form action="user/change_password.php" method="post">
                                                                <div class="form-group">
                                                                    <label class="control-label">Current Password</label>
                                                                    <input type="password" name="currentPassword" class="form-control" /> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">New Password</label>
                                                                    <input type="password" class="form-control" name="newPassword" /> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Re-type New Password</label>
                                                                    <input type="password" class="form-control" name="cpassword" /> </div>
                                                                <div class="margin-top-10">
                                                                    <button type="submit" name="btnsubmit" value="1" class="btn green"> Change Password </a>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PROFILE CONTENT -->
                            </div>
                        </div>
                        <!-- END PAGE BASE CONTENT -->
                    </div>
                </div>
            </div>
            <!-- END SIDEBAR CONTENT LAYOUT -->
        </div>
    </div>
</div>
<!-- BEGIN CORE PLUGINS -->
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#imagedisplay').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#imgInp").change(function() {
            readURL(this);
        });
    });
</script>

<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="../assets/pages/scripts/profile.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="../assets/layouts/layout5/scripts/layout.min.js" type="text/javascript"></script>
        <script src="../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
                    </body>

                    </html>