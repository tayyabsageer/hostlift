<?php
    session_start();
    $username = $_SESSION['login'];
    if($username == null)
    {
        header("location:login.php");
    }
?>

<?php include('section/header.php'); ?>
<div class="wrapper">
<?php include('section/nav_top_header_section.php'); ?>
    <div class="col-md-6">    
         <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3403.3873538203184!2d74.28644345118903!3d31.458529057248445!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3919015e1d7398cd%3A0x3575bac4d817a64!2sBahria%20University%20Lahore%20Campus!5e0!3m2!1sen!2s!4v1605615284866!5m2!1sen!2s" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>
    <div class="col-md-6">
          <div class="page-content-container">
                <div class="page-content-row">
                    <!-- END PAGE SIDEBAR -->
                    <div class="page-content-col">
                        <!-- BEGIN PAGE BASE CONTENT -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet light bordered">
                                                <div class="portlet-title tabbable-line">
                                                    <div class="caption caption-md">
                                                        <i class="icon-globe theme-font hide"></i>
                                                        <span class="caption-subject font-blue-madison bold uppercase">24/7 At Your Service</span>
                                                    </div>
                                                    <ul class="nav nav-tabs">
                                                        <li class="active">
                                                            <a href="#tab_1_1" data-toggle="tab">Contact Us</a>
                                                        </li>

                                                    </ul>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="tab-content">
                                                        <!-- PERSONAL INFO TAB -->
                                                        <div class="tab-pane active" id="tab_1_1">
                                                            <form role="form" action="contact_us_form.php" method="post">
                                                                <div class="form-group">
                                                                    <label class="control-label">Name</label>
                                                                    <input type="text" placeholder="Enter name" class="form-control" value="" name="name"/> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Email</label>
                                                                    <input type="text" placeholder="Enter email" class="form-control" name="email"> </div>
                                                                    <label class="control-label">Description</label>
                                                                    <textarea class="form-control" rows="5" placeholder="Description"
                                                                     name="description"></textarea>
                                                                </div>
                                                                  <div class="margiv-top-10">
                                                                    <button type="submit" class="btn green" name="btnsubmit" value="1">  Send</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PROFILE CONTENT -->
                            </div>
                        </div>
                        <!-- END PAGE BASE CONTENT -->
                    </div>
                </div>
            </div>
    </div>
</div>
<?php include('section/footer.php'); ?>



