<?php ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bootstrap Rating Input Plugin Examples</title>
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.3.1/flatly/bootstrap.min.css">
    <script src="https://use.fontawesome.com/5ac93d4ca8.js"></script>
    <script src="src/bootstrap4-rating-input.js"></script>
    <style type="text/css">
      .container { margin: 150px auto; font-size: 20px; }
    </style>
    <script type="text/javascript">
        $(function () {
            $('input').on('change', function () {
                
                var id  = $(this).attr('id');
                if(id == "rating1")
                { 
                    var i = $(this).val();
                    $("#rate1").val(i);

                }
                else if(id == "rating2")
                {
                    $("#rate2").val($(this).val());
                }
                else if(id == "rating3")
                {
                    $("#rate3").val($(this).val());
                }
                else if(id=="rating4")
                {
                    $("#rate4").val($(this).val());
                }
                else if(id=="rating5")
                {
                    $("#rate5").val($(this).val());
                }
            });
        });
    </script>
</head>
<body>
<div class="container">
    <h1>PLease Rate the Guide</h1>
<p>
    Rate the Knowledge:
    <input type="number" name="inputName" id="rating1" class="rating"
           data-clearable="remove" />
</p>

<p>
    Rate the  Behaviour:
    <input type="number" name="inputName" id="rating2" class="rating"
           data-clearable="remove" />
</p>

<p>
    Rate the Cheefulness:
    <input type="number" name="inputName" id="rating3" class="rating"
           data-clearable="remove" />
</p>
<p>
    Rate the Clearness:
    <input type="number" name="inputName" id="rating4" class="rating"
           data-clearable data-clearable-remain="true"/>
</p>

<p>
    Rate the Follow Timeline:
    <input type="number" name="inputName" id="rating5" class="rating"
           data-clearable data-clearable-remain="true"/>
</p>

<?php $username = $_GET['guide']; ?>
<form method="post" action="submit_rating.php">
<div class="form-group">
    <input type="text" class="form-control" name="guidename" value="<?php echo $username; ?>" id="username" aria-describedby="emailHelp" placeholder="Enter email" hidden >
  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="rate1" id="rate1" aria-describedby="emailHelp" placeholder="Enter email" hidden >
  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="rate2" id="rate2" aria-describedby="emailHelp" placeholder="Enter email" hidden >
  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="rate3" id="rate3" aria-describedby="emailHelp" placeholder="Enter email" hidden>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="rate4" id="rate4" aria-describedby="emailHelp" placeholder="Enter email" hidden>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="rate5" id="rate5" aria-describedby="emailHelp" placeholder="Enter email" hidden>
  </div>
  <button type="submit" class="btn btn-primary btn-block">Submit</button>
</form>

</div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script>
try {
  fetch(new Request("https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js", { method: 'HEAD', mode: 'no-cors' })).then(function(response) {
    return true;
  }).catch(function(e) {
    var carbonScript = document.createElement("script");
    carbonScript.src = "//cdn.carbonads.com/carbon.js?serve=CK7DKKQU&placement=wwwjqueryscriptnet";
    carbonScript.id = "_carbonads_js";
    document.getElementById("carbon-block").appendChild(carbonScript);
  });
} catch (error) {
  console.log(error);
}
</script>
</body>
</html>
