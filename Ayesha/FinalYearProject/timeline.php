<?php
    session_start();
    $username = $_SESSION['login'];
    if($username == null)
    {
        header("location:login.php");
    }
  
?>

<?php include('section/header.php'); ?>
<div class="wrapper">
<?php include('section/nav_top_header_section.php'); ?>
<div class="col-md-12">
    <div class="portlet light portlet-fit bg-inverse ">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-microphone font-red"></i>
                <span class="caption-subject bold font-red uppercase">Discussion Forum</span>
                <span class="caption-helper"></span>
            </div>
            <div class="actions">
            <a href="create_new_discussion.php" class="btn btn-transparent dark btn-outline btn-circle btn-sm active">New Post</a>
                <div class="btn-group btn-group-devided" data-toggle="buttons">
                    <div class="form-group">
                        <select class="form-control" name="discussionid"  id="selecteddiscussion" style="margin-top: 10px;">
                        <option selected disabled>Select the Forum</option>
                        <?php 
                            $sql = "SELECT * FROM discussion_forum";
                            if($result = mysqli_query($con, $sql)){
                            if(mysqli_num_rows($result) > 0){
                                while($row = mysqli_fetch_array($result)){
                            ?>
                                <option value='<?php echo $row['id']; ?>'><?php echo $row['name']; ?></option>
                            <?php
                                }
                            }
                        }
                            ?>
                        </select>
                    </div>    
                </div>
            </div>
        </div>
        <div id="hint" class="row">
                <h1 class="text-Danger" style="    text-align: center;">"Please Select the Discussion Topic from the DropDown"</h1>
            </div>
        <div class="portlet-body">
                    <div class="timeline  white-bg ">
            <?php 
                $sql2 = "SELECT * FROM discussion_forum";
                if($result2 = mysqli_query($con, $sql2)){
                if(mysqli_num_rows($result2) > 0){
                  while($rows = mysqli_fetch_array($result2)){
                    $id = $rows['id'];
                $sql = "SELECT * FROM discussion where discussion_form_id = $id";
                if($result = mysqli_query($con, $sql)){
                if(mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_array($result)){
            ?>
                <div class="timeline-item  rows_arr_<?php echo $id; ?>" id="rows_arr_<?php echo $id; ?>" hidden>
                    <div class="timeline-badge">
                        <img class="timeline-badge-userpic" src="./user/images/<?php echo $row['userimage']; ?>"> </div>
                    <div class="timeline-body">
                        <div class="timeline-body-arrow"> </div>
                        <div class="timeline-body-head">
                            <div class="timeline-body-head-caption">
                                <a href="javascript:;" class="timeline-body-title font-blue-madison"><?php echo $row['username']; ?></a>
                                <span class="timeline-body-time font-grey-cascade">Replied at <?php echo $row['created_at']; ?></span>
                            </div>
                        </div>
                        <div class="timeline-body-content">
                            <span class="font-grey-cascade"><?php echo $row['message_text']; ?> </span>
                        </div>
                    </div>
                </div>
                <?php
                    }
                }
            }
            }
        }

    }
            ?>
            </div>
                <script
  src="https://code.jquery.com/jquery-3.5.1.js"
  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
  crossorigin="anonymous"></script>
                <script>
    
                   $(document).ready(function(){
                       var prev_id= '';
                    $('#selecteddiscussion').on('change', function() {
                         $("#hint").hide();
                         var id = $(this).val();
                         $(".rows_arr_"+prev_id).hide();
                         $(".rows_arr_"+id).show();
                         prev_id = id;
                        

                     });
                   });
                </script>

<?php include('section/footer.php'); ?>
