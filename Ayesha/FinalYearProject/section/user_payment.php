<div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="portlet light bordered" style="    height: 380px !important;" >
                                <div class="portlet-title tabbable-line" >
                                    <div class="caption">
                                        <i class="icon-bubbles font-dark hide"></i>
                                        <span class="caption-subject font-dark bold uppercase">Website User</span>
                                    </div>
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#portlet_comments_1" data-toggle="tab"> User </a>
                                        </li>
                                        <li>
                                            <a href="#portlet_comments_2" data-toggle="tab"> Guide </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="portlet-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="portlet_comments_1">
                                            <!-- BEGIN: Comments -->
                                           
                                                       <?php 
                                                          $sql = "SELECT * FROM user  where role = 'user' limit 5";
                                                          if($result = mysqli_query($con, $sql)){
                                                            if(mysqli_num_rows($result) > 0){
                                                              while($row = mysqli_fetch_array($result)){
                                                       ?>
                                                        <div class="mt-comments">
                                                <div class="mt-comment">
                                                    <div class="mt-comment-img">
                                                        <img src="../assets/pages/media/users/avatar1.jpg" /> </div>
                                                    <div class="mt-comment-body">
                                                        <div class="mt-comment-info">
                                                            <span class="mt-comment-author"><?php echo $row['username']; ?></span>
                                                            <span class="mt-comment-date"><?php echo $row['created_at']; ?></span>
                                                        </div>
                                                        <div class="mt-comment-text"><?php echo $row['description']; ?> </div>
                                                        <div class="mt-comment-details">
                                                            
                                                            <ul class="mt-comment-actions">
                                                            <li>
                                                                    <a href="all_users.php">View</a>
                                                                </li>
                                                                <li>
                                                                    <a href="delete_user_by_admin.php/?id=<?php echo $row['id']; ?>">Delete</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        </div>
                                                </div>
                                            </div>
                                                        <?php
                                                                    }
                                                            }
                                                        }
                                                        ?>
                                                   
                                            <!-- END: Comments -->
                                        </div>
                                        <div class="tab-pane" id="portlet_comments_2">
                                            <!-- BEGIN: Comments -->
                                           
                                                       <?php 
                                                          $sql = "SELECT * FROM user  where role = 'guide' limit 5";
                                                          if($result = mysqli_query($con, $sql)){
                                                            if(mysqli_num_rows($result) > 0){
                                                              while($row = mysqli_fetch_array($result)){
                                                       ?>
                                                        <div class="mt-comments">
                                                <div class="mt-comment">
                                                    <div class="mt-comment-img">
                                                        <img src="user/images/<?php echo $row['image']; ?>" style="    height: 50px;
    width: 50px;" /> </div>
                                                    <div class="mt-comment-body">
                                                        <div class="mt-comment-info">
                                                            <span class="mt-comment-author"><?php echo $row['username']; ?></span>
                                                            <span class="mt-comment-date"><?php echo $row['created_at']; ?></span>
                                                        </div>
                                                        <div class="mt-comment-text"><?php echo $row['description']; ?> </div>
                                                        <div class="mt-comment-details">
                                                            <?php
                                                              if($role == "admin")
                                                              {
                                                                  ?>
                                                                         <ul class="mt-comment-actions">
                                                                <li>
                                                                    <a href="all_users.php">View</a>
                                                                </li>
                                                                <li>
                                                                    <a href="delete_user_by_admin.php/?id=<?php echo $row['id']; ?>">Delete</a>
                                                                </li>
                                                            </ul>
                                                                  <?php
                                                              }
                                                               
                                                             ?>
                                                           
                                                        </div>
                                                        </div>
                                                </div>
                                            </div>
                                                        <?php
                                                                    }
                                                            }
                                                        }
                                                        ?>
                                                    
                                            <!-- END: Comments -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                        <div class="col-md-6 col-sm-6">
                            <div class="portlet light bordered">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption">
                                        <i class="icon-bubbles font-dark hide"></i>
                                        <span class="caption-subject font-dark bold uppercase">Orders</span>
                                    </div>
                                    <ul class="nav nav-tabs">
                                        
                                        <li>
                                            <a href="#portlet_comments_3" data-toggle="tab"> Premium Plan  </a>
                                        </li>
                                        <li>
                                            <a href="#portlet_comments_4" data-toggle="tab"> Gold Plan  </a>
                                        </li>
                                        <li class="active">
                                            <a href="#portlet_comments_5" data-toggle="tab"> Basic Plan </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="portlet-body">
                                    <div class="tab-content">
                                    <div class="tab-pane active " id="portlet_comments_3">
                                            <!-- BEGIN: Comments -->
                                            
                                                       <?php 
                                                          $sql = "SELECT * FROM orders where item_name = 'Premium Plan' limit 5";
                                                          if($result = mysqli_query($con, $sql)){
                                                            if(mysqli_num_rows($result) > 0){
                                                              while($row = mysqli_fetch_array($result)){
                                                       ?>
                                                       <div class="mt-comments">
                                                            <div class="mt-comment">
                                                              <div class="mt-comment-img">
                                                                   <img src="../assets/pages/media/users/avatar1.jpg" /> </div>
                                                                     <div class="mt-comment-body">
                                                                              <div class="mt-comment-info">
                                                            <span class="mt-comment-author"><?php echo $row['name']; ?></span>
                                                            <span class="mt-comment-date"><?php echo $row['created']; ?></span>
                                                        </div>
                                                        <div class="mt-comment-text"><?php echo $row['email']; ?> </div>
                                                        <div class="mt-comment-details">
                                                        
                                                            <ul class="mt-comment-actions">
                                                            <li>
                                                                    <a href="all_users.php">View</a>
                                                                </li>
                                                                <li>
                                                                    <a href="delete_user_by_admin.php/?id=<?php echo $row['id']; ?>">Delete</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        </div>
                                                </div>
                                            </div>
                                                        <?php
                                                                    }
                                                            }
                                                        }
                                                        ?>
                                                    
                                            <!-- END: Comments -->
                                        </div>
                                        <div class="tab-pane " id="portlet_comments_4">
                                            <!-- BEGIN: Comments -->
                                            
                                                       <?php 
                                                          $sql = "SELECT * FROM orders where item_name = 'Basic Plan'  limit 5";
                                                          if($result = mysqli_query($con, $sql)){
                                                            if(mysqli_num_rows($result) > 0){
                                                              while($row = mysqli_fetch_array($result)){
                                                       ?>
                                                       <div class="mt-comments">
                                                            <div class="mt-comment">
                                                              <div class="mt-comment-img">
                                                                   <img src="../assets/pages/media/users/avatar1.jpg" /> </div>
                                                                     <div class="mt-comment-body">
                                                                              <div class="mt-comment-info">
                                                            <span class="mt-comment-author"><?php echo $row['name']; ?></span>
                                                            <span class="mt-comment-date"><?php echo $row['created']; ?></span>
                                                        </div>
                                                        <div class="mt-comment-text"><?php echo $row['email']; ?> </div>
                                                        <div class="mt-comment-details">
                                                           
                                                            <ul class="mt-comment-actions">
                                                            <li>
                                                                    <a href="all_users.php">View</a>
                                                                </li>
                                                                <li>
                                                                    <a href="delete_user_by_admin.php/?id=<?php echo $row['id']; ?>">Delete</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        </div>
                                                </div>
                                            </div>
                                                        <?php
                                                                    }
                                                            }
                                                        }
                                                        ?>
                                                    
                                            <!-- END: Comments -->
                                        </div>
                                        <div class="tab-pane" id="portlet_comments_5">
                                            <!-- BEGIN: Comments -->
                                            
                                                       <?php 
                                                          $sql = "SELECT * FROM orders  where item_name = 'Gold Plan'  limit 5";
                                                          if($result = mysqli_query($con, $sql)){
                                                            if(mysqli_num_rows($result) > 0){
                                                              while($row = mysqli_fetch_array($result)){
                                                       ?>
                                                       <div class="mt-comments">
                                                            <div class="mt-comment">
                                                              <div class="mt-comment-img">
                                                                   <img src="../assets/pages/media/users/avatar1.jpg" /> </div>
                                                                     <div class="mt-comment-body">
                                                                              <div class="mt-comment-info">
                                                            <span class="mt-comment-author"><?php echo $row['name']; ?></span>
                                                            <span class="mt-comment-date"><?php echo $row['created']; ?></span>
                                                        </div>
                                                        <div class="mt-comment-text"><?php echo $row['email']; ?> </div>
                                                        <div class="mt-comment-details">
                                                          
                                                            <ul class="mt-comment-actions">
                                                            <li>
                                                                    <a href="all_users.php">View</a>
                                                                </li>
                                                                <li>
                                                                    <a href="delete_user_by_admin.php/?id=<?php echo $row['id']; ?>">Delete</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        </div>
                                                </div>
                                            </div>
                                                        <?php
                                                                    }
                                                            }
                                                        }
                                                        ?>
                                                    
                                            <!-- END: Comments -->
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>