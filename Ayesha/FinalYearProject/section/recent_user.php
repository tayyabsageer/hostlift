<div class="col-md-6 col-sm-6">
    <div class="portlet light bordered" style="    height: 500px;
    overflow-x: auto;">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-bubble font-dark hide"></i>
                <span class="caption-subject font-hide bold uppercase">Recent Users</span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
        <?php  
     $sql = "SELECT * FROM user where role='user' ORDER BY created_at limit 3 ";
     $result = mysqli_query($con, $sql);
     while($row = mysqli_fetch_array($result)){
        
     
?>
      
                <div class="col-md-4">
                    <!--begin: widget 1-1 -->
                    <div class="mt-widget-1">    
                        <div class="mt-img">
                        <img src="./user/images/<?php echo $row['image']; ?>" style="height: 200px;
    width: 100%;">
                             </div>
                        <div class="mt-body">
                            <h3 class="mt-username"><?php echo $row['name']; ?></h3>
                            <p class="mt-user-title"> <?php echo $row['description']; ?> </p>
                            <div class="mt-stats">
                                <div class="btn-group btn-group btn-group-justified">
                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end: widget 1-1 -->
                </div>
           
        <?php
           }
           ?> 
            </div>
        </div>
    </div>
</div>

<div class="col-md-6 col-sm-6">
    <div class="portlet light bordered" style="    height: 500px;
    overflow-x: auto;">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-bubble font-dark hide"></i>
                <span class="caption-subject font-hide bold uppercase">Recent Guide</span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
        <?php  
     $sql = "SELECT * FROM user where role='guide' ORDER BY created_at limit 3 ";
     $result = mysqli_query($con, $sql);
     while($row = mysqli_fetch_array($result)){
        
     
?>
      
                <div class="col-md-4">
                    <!--begin: widget 1-1 -->
                    <div class="mt-widget-1">    
                        <div class="mt-img">
                        <img src="./user/images/<?php echo $row['image']; ?>" style="height: 200px;
    width: 100%;">
                             </div>
                        <div class="mt-body">
                            <h3 class="mt-username"><?php echo $row['name']; ?></h3>
                            <p class="mt-user-title"> <?php echo $row['description']; ?> </p>
                            <div class="mt-stats">
                                <div class="btn-group btn-group btn-group-justified">
                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end: widget 1-1 -->
                </div>
           
        <?php
           }
           ?> 
            </div>
        </div>
    </div>
</div>
