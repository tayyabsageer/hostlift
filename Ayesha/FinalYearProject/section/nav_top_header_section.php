<?php include("check_user_role.php"); ?>
<header class="page-header">
    <nav class="navbar mega-menu" role="navigation">
        <div class="container-fluid">
            <div class="clearfix navbar-fixed-top">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="toggle-icon">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </span>
                </button>
                <a id="index" class="page-logo" href="index.html">
                    <h1 style="color:white;    margin-top: 3px;">Host Lift</h1>
                </a>
                <form class="search" action="extra_search.html" method="GET">
                    <input type="name" class="form-control" name="query" placeholder="Search...">
                    <a href="javascript:;" class="btn submit md-skip">
                        <i class="fa fa-search"></i>
                    </a>
                </form>
                <div class="topbar-actions">
                    <div class="btn-group-notification btn-group" id="header_notification_bar">
                    </div>
                    <div class="btn-group-red btn-group">
                        <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="fa fa-plus"></i>
                        </button>
                        <ul class="dropdown-menu-v2" role="menu">
                            <li class="active">
                                <a href="timeline.php">New Post</a>
                            </li>
                             <?php 
                                if($role=="admin")
                                {
                                   ?>
                                     <li class="active">
                                <a href="admin_about_us.php">Add About Us</a>
                            </li>
                              <?php
                                }

                                ?>
                          
                        </ul>
                    </div>
                    <div class="btn-group-img btn-group">
                        <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span><?php echo $_SESSION['login']; ?></span>
                            <img src="./user/images/<?php echo $_SESSION['image']; ?>" alt=""> </button>
                        <ul class="dropdown-menu-v2" role="menu">
                            <li>
                                <a href="my_profile.php">
                                    <i class="icon-user"></i> My Profile
                                    <span class="badge badge-danger">1</span>
                                </a>
                            </li>
                            <!-- <li>
                                <a href="lock.php">
                                    <i class="icon-lock"></i> Lock Screen </a>
                            </li> -->
                            <li>
                                <a href="logout.php">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <?php include('navbar.php'); ?>
        </div>
    </nav>
</header>