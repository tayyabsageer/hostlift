<?php

   $users = 0;
   $sql = "SELECT * FROM user";
   $result = mysqli_query($con, $sql);
   while($row = mysqli_fetch_array($result)){
       $users = $users + 1;
   }
   $orders = 0;
   $sql = "SELECT * FROM orders";
   $result = mysqli_query($con, $sql);
   while($row = mysqli_fetch_array($result)){
       $orders = $orders + 1;
   }

   $guide = 0;
   $sql = "SELECT * FROM user where role= 'guide'";
   $result = mysqli_query($con, $sql);
   while($row = mysqli_fetch_array($result)){
       $guide = $guide + 1;
   }

 


?>
<div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-green-sharp">
                                            <span data-counter="counterup" data-value="<?php echo   $users; ?>">0</span>
                                            <small class="font-green-sharp">$</small>
                                        </h3>
                                        <small>TOTAL Users</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: <?php echo  $users; ?>>%;" class="progress-bar progress-bar-success green-sharp">
                                            <span class="sr-only"><?php echo $users; ?>% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> progress </div>
                                        <div class="status-number"> <?php echo   $users; ?>% </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-green-sharp">
                                            <span data-counter="counterup" data-value="<?php echo   $orders; ?>">0</span>
                                            <small class="font-green-sharp">$</small>
                                        </h3>
                                        <small>TOTAL Orders</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: <?php echo  $orders; ?>>%;" class="progress-bar progress-bar-success green-sharp">
                                            <span class="sr-only"><?php echo $orders; ?>% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> progress </div>
                                        <div class="status-number"> <?php echo   $orders; ?>% </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-green-sharp">
                                            <span data-counter="counterup" data-value="<?php echo   $guide; ?>">0</span>
                                            <small class="font-green-sharp">$</small>
                                        </h3>
                                        <small>TOTAL Guides</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: <?php echo  $guide; ?>>%;" class="progress-bar progress-bar-success green-sharp">
                                            <span class="sr-only"><?php echo $guide; ?>% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> progress </div>
                                        <div class="status-number"> <?php echo   $guide; ?>% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>