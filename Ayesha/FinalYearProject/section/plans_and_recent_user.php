<?php

   $users = 0;
   $sql = "SELECT * FROM user where role='user'";
   $result = mysqli_query($con, $sql);
   while($row = mysqli_fetch_array($result)){
       $users = $users + 1;
   }
   $orders = 0;
   $sql = "SELECT * FROM orders";
   $result = mysqli_query($con, $sql);
   while($row = mysqli_fetch_array($result)){
       $orders = $orders + 1;
   }

   $guide = 0;
   $sql = "SELECT * FROM user where role='guide'";
   $result = mysqli_query($con, $sql);
   while($row = mysqli_fetch_array($result)){
       $guide = $guide + 1;
   }


   $basic = 0;
   $sql = "SELECT * FROM orders where item_name='Basic Plan'";
   $result = mysqli_query($con, $sql);
   while($row = mysqli_fetch_array($result)){
       $basic = $basic + 1;
   }
   $pre = 0;
   $sql = "SELECT * FROM orders  where item_name='Premium Plan'";
   $result = mysqli_query($con, $sql);
   while($row = mysqli_fetch_array($result)){
       $pre = $pre + 1;
   }

   $gold = 0;
   $sql = "SELECT * FROM orders where item_name='Gold Plan'";
   $result = mysqli_query($con, $sql);
   while($row = mysqli_fetch_array($result)){
       $gold = $gold + 1;
   }

 


?>
<div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-cursor font-dark hide"></i>
                                        <span class="caption-subject font-dark bold uppercase">General Stats</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="easy-pie-chart">
                                                <div class="number transactions" data-percent="<?php echo $users; ?>">
                                                    <span><?php echo $users; ?></span>% </div>
                                                <a class="title" href="javascript:;"> User
                                                    <i class="icon-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="margin-bottom-10 visible-sm"> </div>
                                        <div class="col-md-4">
                                            <div class="easy-pie-chart">
                                                <div class="number visits" data-percent="<?php echo $guide; ?>">
                                                    <span><?php echo $guide; ?></span>% </div>
                                                <a class="title" href="javascript:;"> Guide
                                                    <i class="icon-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="margin-bottom-10 visible-sm"> </div>
                                        <div class="col-md-4">
                                            <div class="easy-pie-chart">
                                                <div class="number bounce" data-percent="<?php echo $orders; ?>">
                                                    <span><?php echo $orders; ?></span>% </div>
                                                <a class="title" href="javascript:;"> Payment
                                                    <i class="icon-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-cursor font-dark hide"></i>
                                        <span class="caption-subject font-dark bold uppercase">Subscription Status</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="easy-pie-chart">
                                                <div class="number transactions" data-percent="<?php echo $basic; ?>">
                                                    <span><?php echo $basic; ?></span>% </div>
                                                <a class="title" href="javascript:;"> Basic Plan
                                                    <i class="icon-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="margin-bottom-10 visible-sm"> </div>
                                        <div class="col-md-4">
                                            <div class="easy-pie-chart">
                                                <div class="number visits" data-percent="<?php echo $pre; ?>">
                                                    <span><?php echo $pre; ?></span>% </div>
                                                <a class="title" href="javascript:;"> Preiumn Plan 
                                                    <i class="icon-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="margin-bottom-10 visible-sm"> </div>
                                        <div class="col-md-4">
                                            <div class="easy-pie-chart">
                                                <div class="number bounce" data-percent="<?php echo $gold; ?>">
                                                    <span><?php echo $gold; ?></span>% </div>
                                                <a class="title" href="javascript:;"> Gold Plan
                                                    <i class="icon-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php include('stripe_payment.php'); ?>
                      
                    </div>