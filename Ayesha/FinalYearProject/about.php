<?php
    session_start();
    $username = $_SESSION['login'];
    if($username == null)
    {
        header("location:login.php");
    }
    
?>

<?php include('section/header.php'); ?>

<div class="wrapper">
<?php include('section/nav_top_header_section.php'); ?>
  <?php
  $sql2 = "SELECT * FROM city";
  if($result2 = mysqli_query($con, $sql2)){
  if(mysqli_num_rows($result2) > 0){
    while($rows = mysqli_fetch_array($result2)){
      $id = $rows['id'];

      ?>
<div class="col-md-12 col-sm-12">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-bubble font-dark hide"></i>
                <span class="caption-subject font-hide bold uppercase"><h1>Place's To see in <?php echo $rows['title']; ?></h1></span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
              <?php
                   $sql = "SELECT * FROM city_place where city_id = $id";
                   if($result = mysqli_query($con, $sql)){
                   if(mysqli_num_rows($result) > 0){
                       while($row = mysqli_fetch_array($result)){
               ?>
                <div class="col-md-4">
                    <!--begin: widget 1-1 -->
                    <div class="mt-widget-1">
                        <div class="mt-icon">
                        </div>
                        <div class="mt-img">
                            <img src="images/<?php echo $row['imagename']; ?>" style="    width: 100%;
    height: 263px;"> </div>
                        <div class="mt-body">
                            <h3 class="mt-username"><?php echo $row['title']; ?></h3>
                            <p class="mt-user-title"><?php echo $row['message']; ?> </p>
                            <div class="mt-stats">
                                <div class="btn-group btn-group btn-group-justified">
                                    <a href="javascript:;" class="btn font-red">
                                        <i class="icon-map"></i><?php echo $row['location']; ?> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if($role == "admin")
                     {
                         ?>
                    <div class="col-6 col-sm-6">
                    <!-- <a href="">Delete</a> -->
                    </div>
                    <div class="col-6 col-sm-6">
                    <a href="delete_place.php?id=<?php echo $row['id']; ?>">Delete</a>
                    </div>
                    <?php
                     }
                     ?>
                   
                </div>
                <?php
                       }
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>
  <?php
    }

}
  }
  ?>
</div>
<?php include('section/footer.php'); ?>
